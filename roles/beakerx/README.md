BeakerX
=========

A community project created by [TwoSigma](http://twosigma.github.io/), [BeakerX](https://github.com/twosigma/beakerx) is a collection of JVM kernels and interactive widgets for plotting, tables, autotranslation, and other extensions to Jupyter Notebook.

Requirements
------------

None

Role Variables
--------------
* `application_name`: Name to use for the deployed pods, database and Jupyter application
* `notebook_memory`: Amount of memory to allocate to each notebook pod
* `notebook_cpu`: Amount of cpu to allocate to each notebook pod
* `volume_size`: Size of the persistent volume to attach to each notebook pod

Dependencies
------------

None

Example Variable Spec
---------------------
```
beakerx:
  odh_deploy: true
  application_name: odh-beakerx
  notebook_memory: 2Gi
  notebook_cpu: 2
  volume_size: 2Gi
```
License
-------

Apache License 2.0

Author Information
------------------
* [Open Data Hub Contributors](mailto:contributors@lists.opendatahub.io)
* [BeakerX Community Project](https://github.com/twosigma/beakerx)
