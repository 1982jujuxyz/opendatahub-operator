
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="using-beakerx"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Using Beakerx
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

A community project created by link:http://twosigma.github.io/[TwoSigma], link:https://github.com/twosigma/beakerx[BeakerX] is a collection of JVM kernels and interactive widgets for plotting, tables, autotranslation, and other extensions to Jupyter Notebook.  BeakerX provides JVM support, Spark cluster support, polyglot programming, interactive plots, tables, forms, publishing, and more.  The Open Data Hub deployment of BeakerX deploys as a standalone Jupyter notebook pod with the BeakerX extension v1.4.0 enabled.

BeakerX has a great collection of notebooks that demonstrate all of the available features it provides. Below are instructions that will show you how to load these samples notebook on the BeakerX Jupyter instance deployed by the Open Data Hub.

.Prerequisites

* A successful deployment of BeakerX in an Open Data Hub deployment

.Procedure

. Open a web browser and navigate to the route that was created for BeakerX in your deployment of the Open Data Hub.
. Open a new terminal in the BeakerX Jupyter webui by clicking on the `New` dropdown button and select `Terminal` from the dropdown menu.
. Run the following command in the terminal window to clone the link:https://github.com/twosigma/beakerx[BeakerX] repository to the Jupyter instance. In this directory, you will use the BeakerX link:https://raw.githubusercontent.com/twosigma/beakerx/master/StartHere.ipynb[Start Here notebook] and link:https://github.com/twosigma/beakerx/tree/master/doc[sample docs] folder to explore the many features of BeakerX.
+
....
git clone https://github.com/twosigma/beakerx
....

. Exit the terminal and return to the BeakerX Jupyter webui file listing window
. At this point you can view the available BeakerX example notebooks by executing the notebook at `beakerx/StartHere.ipynb`

NOTE: Some sample notebooks may reference data files that are available in the `beakerx/docs` folder and relative to location of the sample notebook.  If you move any of the sample notebooks out of the `beakerx/docs` folder, you may need to edit the path to the data file in the sample notebook.

//.Verification steps
//(Optional) Provide the user with verification method(s) for the procedure, such as expected output or commands that can be used to check for success or failure.

.Additional resources

* link:http://beakerx.com[BeakerX]
* link:https://github.com/twosigma/beakerx/blob/master/doc/Cheatsheet.pdf[BeakerX Cheat Sheet]
* link:http://github.com/twosigma/beakerx[BeakerX Github Repo]
* link:https://jupyter-notebook.readthedocs.io[Jupyter Notebook Documentation]
