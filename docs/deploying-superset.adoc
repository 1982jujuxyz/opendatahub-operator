// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="deploying-superset"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Deploying Superset Setup
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

This procedure describes how to enable (and disable) Superset for the Open Data Hub

.Prerequisites

* A terminal shell with the OpenShift client command `oc` availabe.
* A copy of the files in this repository available in your terminal shell.
* The ability to run the operator as outlined at link:manual-installation.adoc[manual-installation]

.Procedure

. Change directory to your copy of the repository.
. Run the manual-installation steps 1 - 5 as outlined in this link:manual-installation.adoc[manual-installation]
. Copy the default Open Data Hub custom resource manifest, copying this file will allow you to make changes for your environment without disturbing the original file
+
....
# Make sure you're in the root folder of opendatahub-operator
cp deploy/crds/opendatahub_v1alpha1_opendatahub_cr.yaml my_environment_cr.yaml
....

. Open the newly created `my_environment_cr.yaml`. Setting `odh_deploy` to `true` or `false` within the `superset` field will either enable or disable Superset. Edit the following lines in the file to enable it.
+
....
  superset:
    odh_deploy: true
....
. You can further configure the Superset CR Spec by adding/changing the following fields, the example below shows their default settings
+
....
  superset:
    odh_deploy: true

    # Superset Version
    version: 0.34.0

    # Create an admin user
    superset_admin:
      admin_usr: userKPJ
      admin_psw: 7ujmko0
      admin_fname: admin
      admin_lname: admin
      admin_email: admin@fab.org

    # Your App secret key
    secret_key: thisISaSECRET_1234

    # The data volume is where you would mount your SQLite file or a volume to collect any logs that are routed there
    data_volume_size: 512Mi

    # Change to an external sql backend (by default sqlite is used)
    sqlalchemy_db_uri: sqlite:////var/lib/superset/superset.db

....
. Deploy the Open Data Hub using the custom resource manifest for your
  environment by entering the following command.
+
....
oc apply -f my_environment_cr.yaml
....
. This will deploy a default Superset instance alongside any other projects you have defined. Visit the newly created route for the application and login using the admin credentials you provided in the CR above.

.Verification steps
To verify installation is successful:

. Check to see that the Superset pod is running without errors.
. Verify that there is a route exposed for Superset and there are no errors reported
. Visit the route and see if the login page appears, enter the credentials provided in the CR and ensure you can log in (in the example above the credentials would be: `userKPJ`/`7ujmko0`)


Once you log in you will see a blank page with the Superset logo, as there are no dashboards created, this is expected.

The following steps illustrate how you may quickly setup a new PostgreSQL DB and connect it with Superset via the UI and build a basic chart.

. Ensure you are in the same namespace as where Superset is deployed
. Run the following oc command to spruce up an ephemeral PostgreSQL:
+
....
oc new-app \
    -e POSTGRESQL_USER=userEKY \
    -e POSTGRESQL_PASSWORD=UQpmMreb7SsJ3aLi \
    -e POSTGRESQL_DATABASE=superset \
    openshift/postgresql:9.6
....
. Now that we have a postgres instance running, we will setup some sample tables within the superset database using pgbench, to do this first `oc rsh` into the postgres pod.
+
....
# Get the postgresql pod name:
$ oc get pods
NAME                                  READY     STATUS    RESTARTS   AGE
opendatahub-operator-d79ff8bb-9cbxc   1/1       Running   0          3h
postgresql-1-hlzkz                    1/1       Running   0          8m
superset-1-2sqql                      1/1       Running   0          3h

# Rsh into the pod
$ oc rsh postgresql-1-hlzkz

# Use pgbench to generate some sample tables with generic data
sh-4.2$ pgbench -i superset

# Use psql to grant permissions to run SELECT queries for your PostgreSQL user
# This is required to be able to run queries from Superset
sh-4.2$ psql -d superset

# Once in the psql command line, enter the following (Note: Replace the username with the one you supplied with the CR)
superset=# GRANT SELECT ON ALL TABLES IN SCHEMA public TO "userEKY";
....
. Now that we have a database with some data, open the Superset route and login using your credentials
. Check for the `Sources` tab at the top, navigate towards `Sources > Databases`
. You will see a green plus (+) icon at the top right, click this icon to add a new database, you will be forwarded to a form
. Give this database a name to identify it in Superset
. Enter your DB URI under `SQLAlchemy URI`, following our example, this will look like the following:
+
....
postgresql://userEKY:UQpmMreb7SsJ3aLi@postgresql:5432/superset
....
+
Note that `postgresql:5432` is the `<service_name>:<port>` in Openshift for PostgreSQL

. Click the test connection to ensure that the DB is reachable by Superset
. Scroll down and click save at the bottom
. Navigate to `Sources > Tables` at the top and click the green plus (+) icon at the top right to add a table
. A form should pop up, select the Database name you specified (Step 7) and within `table name` enter `pgbench_tellers`
. Leave schema blank (we're using the public schema by default) and click save
. Goto `Charts` at the top and click the green plus (+) to add a new chart
. Pick `pgbench_tellers` as the datasource and choose `Pie Chart` as your visualization, then proceed to click `Create New Chart`
. You will be navigated to a new page with a form on the left, within that form is a `Group by` field, click its drop down and select `tid` (not a very meaningful `group by` but for our purposes it suffices)
. Click `Run Query` at the top left, if the DB connectivity works - you will see a pie chart on the right side equally divided
. Click `Save` on the top left and give your chart a name and create a new dashboard via `Add to new dashboard`
. You will have created a new dashboard with a new chart that queries your PostgreSQL DB

.Additional resources

* More information about *Superset* can be found link:https://superset.incubator.apache.org/[here].
* The *Superset* upstream repo can be found link:https://github.com/apache/incubator-superset[here].